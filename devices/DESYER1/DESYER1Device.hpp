/**
 * Caribou implementation for the DESYER1 chip
 */

#pragma once

#include "device/CaribouDevice.hpp"
#include "hardware_abstraction/carboard/Carboard.hpp"

#include "DESYER1Defaults.hpp"

namespace caribou {

  /** DESYER1 Device class definition
   */
  class DESYER1Device : public CaribouDevice<carboard::Carboard, iface_mem> {

  public:
    DESYER1Device(const caribou::Configuration config);
    ~DESYER1Device(){};

    /** Initializer function for DESYER1
     * Sets slow control-controlled values from config:
     *    - pixel mask
     *    - krum_bias (Krummenacher bias trimming)
     *    - thresh_trimming (Comparator threshold trimming)
     */
    void configure() override;

    /** Turn on the Caribou-supplied biases for the DESYER1 chip
     */
    void powerUp() override;

    /** Turn off the DESYER1 power
     */
    void powerDown() override;

    /** Start the data acquisition
     */
    void daqStart() override;

    /** Stop the data acquisition
     */
    void daqStop() override;

    pearydata getData() override;
    pearyRawData getRawData() override;

    void chipReset();

  private:
    // Functions
    void configurePixel(int pix_number, bool pix_power, double krum_bias, double thresh_trimming);

    /** Lookup Table for slow-control values
     * Decide later on which of these will be used. most likely int->b
     * TODO: Update these maps with the correct ones. Sara has them. ~Jona
     */
    const std::map<int, unsigned int> thresh_trimming_map_int_b{{7, 0b0000},
                                                                {6, 0b0001},
                                                                {5, 0b0010},
                                                                {4, 0b0011},
                                                                {3, 0b0100},
                                                                {2, 0b0101},
                                                                {1, 0b0110},
                                                                {0, 0b0111},
                                                                {-1, 0b1001},
                                                                {-2, 0b1010},
                                                                {-3, 0b1011},
                                                                {-4, 0b1100},
                                                                {-5, 0b1101},
                                                                {-6, 0b1110},
                                                                {-7, 0b1111}};
    const std::map<double, unsigned int> thresh_trimming_map_val_b{{370.6, 0b0000},
                                                                   {318.8, 0b0001},
                                                                   {266.4, 0b0010},
                                                                   {213.8, 0b0011},
                                                                   {161.0, 0b0100},
                                                                   {107.8, 0b0101},
                                                                   {54.3, 0b0110},
                                                                   {0, 0b0111},
                                                                   {-54.3, 0b1001},
                                                                   {-107.8, 0b1010},
                                                                   {-161.0, 0b1011},
                                                                   {-213.8, 0b1100},
                                                                   {-266.4, 0b1101},
                                                                   {-318.8, 0b1110},
                                                                   {-370.6, 0b1111}};

    const std::map<double, unsigned int> krum_bias_map_val_b{{9.91, 0b000},
                                                             {8.96, 0b001},
                                                             {7.73, 0b010},
                                                             {6.78, 0b011},
                                                             {5.53, 0b100},
                                                             {4.57, 0b101},
                                                             {3.30, 0b110},
                                                             {2.32, 0b111}};
    const std::map<double, unsigned int> krum_bias_map_int_b{
      {7, 0b000}, {6, 0b001}, {5, 0b010}, {4, 0b011}, {3, 0b100}, {2, 0b101}, {1, 0b110}, {0, 0b111}};
  }; // class DESYER1Device

} // namespace caribou
