#pragma once

#include "utils/dictionary.hpp"

namespace caribou {

/** Default device path for this device: SEAF-connector I2C bus on BUS_I2C2
 */
//#define DEFAULT_DEVICEPATH caribou::carboard::BUS_I2C2

/** Default currents and biases
 * Current have uA resolution at best. Using floats makes no sense, will be cast to int anyways.
 */
#define DESYER1_DEFAULT_I2C 95  // A => 95uA
#define DESYER1_DEFAULT_IKRUM 2 // A => default 2.1uA ; caribou has uA precision, use int
#define DESYER1_DEFAULT_ICSA 2  // A => default 2.4uA ; caribou has uA precision, use int

#define DESYER1_DEFAULT_VDUMMYPIX 0.35            // V => 0.35V
#define DESYER1_DEFAULT_VDUMMYPIX_CURRENT 0.1     // A - to be tested!
#define DESYER1_DEFAULT_VKRUMREF 0.325            // V
#define DESYER1_DEFAULT_VKRUMREF_CURRENT .0001    // A - to be tested!
#define DESYER1_DEFAULT_VCOMPTHRESH 0.325         // V
#define DESYER1_DEFAULT_VCOMPTHRESH_CURRENT .0001 // A - to be tested!

#define DESYER1_CMOS_OUT_1_TO_4 1.2
#define DESYER1_CMOS_IN_1_TO_4 1.2
  /** Defining registers --- i.e. everything we want to write to or read from the CHIP.
   *
   * NOTE: I just straight up copied this from H2M. Hope it helps.
   * NOTE: We could also use one 10-bit register instead of splitting the data up as I did. Whatever works better. ~Jona
   * NOTE: I dont understand how the reset registers work. left that to the Pros ~Jona
   *
   *      pix_adress - 2 bit
   *      pix_power - 1 bit
   *      krum_bias - 3 bit
   *      thresh_trimming - 4 bit
   *
   * Define address space.
   * Each address corresponds to a 32-bit memory block.
   * We have 64 address for each: CHIP, ROUT, and CTRL.
   * The READOUT_MAP_SIZE gives the size of the address space associated with the memory.
   */
  const intptr_t CORE_BASE_ADDRESS = 0x43C70000;
  const intptr_t CORE_LSB = 2;
  const size_t CORE_FPGA_MEM_SIZE = 4 << CORE_LSB;

  const intptr_t DESYER1_FPGA_REG_CTRL = 0 << CORE_LSB;
  const intptr_t FPGA_CHIPCTRL1 = 1 << CORE_LSB;
  const intptr_t FPGA_CHIPCTRL2 = 2 << CORE_LSB;
  const intptr_t DESYER1_FPGA_DEBUG = 3 << CORE_LSB;

  const memory_map DESYER1_MEM{CORE_BASE_ADDRESS, CORE_FPGA_MEM_SIZE, PROT_READ | PROT_WRITE};

  // clang-format off
#define DESYER1_MEMORY \
{ \
  {"fpga_rst",    {DESYER1_MEM, register_t<size_t>(DESYER1_FPGA_REG_CTRL, 0x00000001, false, true,  false)}}, \
  {"sr_divider",  {DESYER1_MEM, register_t<size_t>(DESYER1_FPGA_REG_CTRL, 0xFFFF0000, true,  true,  false)}}, \
  {"chip_rst",    {DESYER1_MEM, register_t<size_t>(FPGA_CHIPCTRL1,        0x00000001, false, true,  false)}}, \
  {"sr_idle",     {DESYER1_MEM, register_t<size_t>(FPGA_CHIPCTRL1,        0x00000002, true,  true,  false)}}, \
  {"sr_address",  {DESYER1_MEM, register_t<size_t>(FPGA_CHIPCTRL1,        0x00000300, true,  true,  false)}}, \
  {"sr_data",     {DESYER1_MEM, register_t<size_t>(FPGA_CHIPCTRL2,        0x000000FF, true,  true,  false)}}, \
  {"debug1",      {DESYER1_MEM, register_t<size_t>(DESYER1_FPGA_DEBUG,    0x0000000F, true,  true,  false)}},  \
  {"debug2",      {DESYER1_MEM, register_t<size_t>(DESYER1_FPGA_DEBUG,    0x000000F0, true,  true,  false)}}  \
}

#define DESYER1_REGISTERS \
{ \
  {"pix_address",      register_t<size_t>(FPGA_CHIPCTRL1, 0x00000300, true,  true,  false)}, \
  {"pix_power",        register_t<size_t>(FPGA_CHIPCTRL2, 0x00000001, true,  true,  false)}, \
  {"krum_bias",        register_t<size_t>(FPGA_CHIPCTRL2, 0x0000000E, true,  true,  false)}, \
  {"thresh_trimming",  register_t<size_t>(FPGA_CHIPCTRL2, 0x000000F0, true,  true,  false)}  \
}
  // clang-format on

} // namespace caribou
