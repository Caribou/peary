#include "Device.hpp"
#include "config.h"
#include "peary/utils/log.hpp"

bool caribou::Device::managedDevice = false;

using namespace caribou;

Device::Device(const caribou::Configuration&) {

  LOG(STATUS) << "New Caribou device instance, version " << getVersion();

  if(Device::isManaged()) {
    LOG(STATUS) << "This device is managed through the device manager.";
  } else {
    LOG(STATUS) << "Unmanaged device.";

    // Check for running device manager:
    if(check_flock("/pearydevmgr")) {
      LOG(WARNING) << "It looks like a Peary Device Manager is already running.\n"
                   << "If this is not the case, run 'peary_clear_semaphores' to reset";
      throw caribou::caribouException("Found running device manager instance.");
    }

    // Acquire lock for CaribouDevice:
    if(!acquire_flock("/pearydev")) {
      LOG(WARNING) << "It looks like a Peary Device is already running.\n"
                   << "If this is not the case, run 'peary_clear_semaphores' to reset";
      throw caribou::caribouException("Found running device instance.");
    }
  }
}

Device::~Device() {
  release_flock("/pearydev");
}

std::string Device::getVersion() {
  return std::string(PACKAGE_STRING);
}

std::vector<std::pair<std::string, std::size_t>> Device::listCommands() {
  return _dispatcher.commands();
}

std::string Device::command(const std::string& name, const std::vector<std::string>& args) {
  try {
    return _dispatcher.call(name, args);
  } catch(std::invalid_argument& e) {
    throw caribou::ConfigInvalid(e.what());
  }
}

std::string Device::command(const std::string& name, const std::string& arg) {
  return command(name, std::vector<std::string>{arg});
}
